<?php

/**
 * Product:       Xtento_ProductExport
 * ID:            Xyaqh8j3TBU6x9sP6dAL+txUu77+FV5yzNWD/S45MnI=
 * Last Modified: 2016-04-14T15:37:35+00:00
 * File:          app/code/Xtento/ProductExport/registration.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Xtento_ProductExport',
    __DIR__
);
