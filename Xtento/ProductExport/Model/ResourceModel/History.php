<?php

/**
 * Product:       Xtento_ProductExport
 * ID:            Xyaqh8j3TBU6x9sP6dAL+txUu77+FV5yzNWD/S45MnI=
 * Last Modified: 2016-04-14T15:37:57+00:00
 * File:          app/code/Xtento/ProductExport/Model/ResourceModel/History.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\ProductExport\Model\ResourceModel;

class History extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('xtento_productexport_profile_history', 'history_id');
    }
}
